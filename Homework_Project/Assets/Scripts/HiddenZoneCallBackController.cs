﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HiddenZoneCallBackController : MonoBehaviour {
    public GameObject callbackPositionObject;

    private bool onDoor;
    private Rigidbody2D playerRigidbody;

    // Use this for initialization
    void Start()
    {
        onDoor = false;
        playerRigidbody = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (onDoor && Input.GetAxisRaw("Vertical") > 0)
        {
            playerRigidbody.MovePosition(callbackPositionObject.transform.position);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            onDoor = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            onDoor = false;
        }
    }
}
