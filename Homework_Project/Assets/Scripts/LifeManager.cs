﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class LifeManager : MonoBehaviour {

    // public int startingLives;
    public GameObject gameOverScreen;
    public Text gameOverText;
    public string mainMenu;
    public float waitAfterGameOver;

    private static int lifeCounter;
    //private Text theText;
    private PlayerController player;

    // Use this for initialization
    void Start()
    {
        mainMenu = "TitleMenu";
        //this.theText = GetComponent<Text>();
        lifeCounter = 1;
        this.player = FindObjectOfType<PlayerController>();
        gameOverText.text = "Game Over";
    }

    // Update is called once per frame
    void Update()
    {
        if (lifeCounter <= 0)
        {
            this.gameOverScreen.SetActive(true);
            this.player.gameObject.SetActive(false);
            
        }

        //this.theText.text = "x " + lifeCounter;

        if (this.gameOverScreen.activeSelf)
        {
            this.waitAfterGameOver -= Time.deltaTime;
            //Advertisement.Show();
        }

        if (this.waitAfterGameOver < 0)
        {
            SceneManager.LoadScene(mainMenu);
        }
    }

    public void GiveLife()
    {
        lifeCounter++;
    }

    public void TakeLife()
    {
        Debug.Log("생명점 감소");
        lifeCounter--;
    }

    public void BossKill()
    {
        
        
            SceneManager.LoadScene("GameClear");
        
    }
}
