﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class FallDeathController : MonoBehaviour {

    private LifeManager lifeManager;

    private void Start()
    {
        lifeManager = FindObjectOfType<LifeManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("플레이어 낙사");
            HealthManager.KillPlayer();
        }

        if (other.tag == "Boss")
        {
            Debug.Log("보스 낙사");
            lifeManager.BossKill();
        }
    }
}
