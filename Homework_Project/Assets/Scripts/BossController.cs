﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour {

    private Rigidbody2D bossRigidBody;

	// Use this for initialization
	void Start () {
        this.bossRigidBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //보스의 트리거에 플레이어가 들어오면
    private void OnTriggerEnter2D(Collider2D other)
    {
        //플레이어를 포착
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("플레이어를 찾았음");
        }

    }

    //트리거 안에서 플레이어가 머무르면
    private void OnTriggerStay2D(Collider2D other)
    {
        //플레이어에게 접근
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("거만한 플레이어가 도망가질 않음");
            bossRigidBody.velocity = new Vector2(3, 0);
        }
    }

    //트리거 밖으로 플레이어가 나가면
    private void OnTriggerExit2D(Collider2D other)
    {
        //플레이어를 못찾음
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("플레이어가 도망감");
            bossRigidBody.velocity = new Vector2(0, 0);
        }
    }

    //무엇인가 충돌하면
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("보스와 플레이어가 충돌");
        }

        if (other.gameObject.tag == "PlayersSword")
        {
            Debug.Log("보스와 플레이어의 칼에 충돌");
        }
    }
}
