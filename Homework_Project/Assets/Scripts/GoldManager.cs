﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldManager : MonoBehaviour {

    public static int gold;
    public Text text;

    public static void AddGolds(int goldsToAdd)
    {
        gold += goldsToAdd;
    }

    public static void Reset()
    {
        gold = 0;
    }

    void Start()
    {
        this.text = GetComponent<Text>();
        gold = 0;

    }

    void Update()
    {
        if (gold < 0)
        {
            gold = 0;
        }

        this.text.text = "" + gold;
    }
}
