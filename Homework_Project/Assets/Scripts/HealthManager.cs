﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

    public static int playerHealth;

    public int maxPlayerHealth;
    public bool isDead;
    public Text text;
    public GameObject deathEffect;

    private LifeManager lifeSystem;
    private TimeManager theTime;

    // Use this for initialization
    void Start()
    {
        //HealthManager.playerHealth = this.maxPlayerHealth;
        //HealthManager.playerHealth = PlayerPrefs.GetInt("PlayerCurrentHealth");
        HealthManager.playerHealth = 3;
        this.isDead = false;
        this.lifeSystem = FindObjectOfType<LifeManager>();
        this.theTime = FindObjectOfType<TimeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (HealthManager.playerHealth < 1 && !this.isDead)
        {
            Debug.Log("플레이어 사망처리");
            HealthManager.playerHealth = 0;
            this.isDead = true;

            StartCoroutine(PlayerDeathEffect());        //플레이어 파괴 파티클 play

            this.lifeSystem.TakeLife();
            this.theTime.ResetTime();
        }

        this.text.text = "X " + HealthManager.playerHealth;
    }

    public static void HurtPlayer(int damageToGive)
    {
        HealthManager.playerHealth -= damageToGive;
        PlayerPrefs.SetInt("PlayerCurrentHealth", HealthManager.playerHealth);
    }

    public void FullHealth()
    {
        HealthManager.playerHealth = PlayerPrefs.GetInt("PlayerMaxHealth");
        PlayerPrefs.SetInt("PlayerCurrentHealth", HealthManager.playerHealth);
    }

    public static void KillPlayer()
    {
        Debug.Log("플레이어 사망처리_KillPlayer");
        HealthManager.playerHealth = 0;
        PlayerPrefs.SetInt("PlayerCurrentHealth", 0);
    }

    IEnumerator PlayerDeathEffect()
    {
        Instantiate(this.deathEffect, transform.position, transform.rotation);
        yield return new WaitForSeconds(1.0f);
    }
}
