﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour {

    public int coinToPoint;
    public int coinToGold;
    public AudioSource coinSoundEffect;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>() == null)
        {
            return;
        }

        ScoreManager.AddPoints(coinToPoint);
        GoldManager.AddGolds(coinToGold);
        coinSoundEffect.Play();
        Destroy(gameObject);
    }
}
