﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public static int score;
    public Text text;

    public static void AddPoints(int pointsToAdd)
    {
        score += pointsToAdd;
    }

    public static void Reset()
    {
        score = 0;
    }

    void Start()
    {
        this.text = GetComponent<Text>();
        score = 0;

    }

    void Update()
    {
        if (score < 0)
        {
            score = 0;
        }

        this.text.text = "" + score;
    }


}
