﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class BackToTitleController : MonoBehaviour {

    private const string titleSceneName = "TitleMenu";

    public void OnClickBackToTitle()
    {
        SceneManager.LoadScene(titleSceneName);
    }

    public void OnShowAD()
    {
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(options);
    }

#if UNITY_ADS
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                SceneManager.LoadScene(titleSceneName);
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                SceneManager.LoadScene(titleSceneName);
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                SceneManager.LoadScene(titleSceneName);
                break;
        }
    }
#endif
}
