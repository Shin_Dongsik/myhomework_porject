﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformController : MonoBehaviour {

    public GameObject platform;
    public float moveSpeed;
    public int pointSelection;
    public Transform[] points;

    private Transform currentPoint;

    // Use this for initialization
    void Start () {
        currentPoint = points[pointSelection];
    }
	
	// Update is called once per frame
	void Update () {
        platform.transform.position = Vector2.MoveTowards(this.platform.transform.position, this.currentPoint.position, Time.deltaTime * this.moveSpeed);

        if (platform.transform.position == currentPoint.position)
        {
            pointSelection++;
            if (pointSelection == points.Length)
            {
                pointSelection = 0;
            }
            currentPoint = points[pointSelection];

        }
    }
}
