﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {

    public float startingTime;

    private Text theText;
    private float countingTime;

    // Use this for initialization
    void Start()
    {
        this.theText = GetComponent<Text>();
        this.countingTime = this.startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        this.countingTime -= Time.deltaTime;

        this.theText.text = "" + Mathf.Round(this.countingTime);
    }

    public void ResetTime()
    {
        this.countingTime = this.startingTime;
    }
}
