﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayerOnContact : MonoBehaviour {

    public int mobAttackPower;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("플레이어 공격당함");
            HealthManager.HurtPlayer(this.mobAttackPower);
            //HealthManager.KillPlayer();
            var audio = other.GetComponent<AudioSource>();
            audio.Play();
            var player = other.GetComponent<PlayerController>();
            player.knockbackCount = player.knockbackLength;
            if (other.transform.position.x < transform.position.x)
            {
                player.knockFromRight = true;
            }
            else
            {
                player.knockFromRight = false;
            }
        }
    }
}
