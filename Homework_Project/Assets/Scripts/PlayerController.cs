﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float jumpHeight;
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    public bool onLadder;
    public float climbSpeed;
    public float knockback;
    public float knockbackLength;
    public bool knockFromRight;
    public float knockbackCount;

    private Vector3 movement;
    private Rigidbody2D playerRigidbody;
    private SpriteRenderer spriteRenderer;
    private Animator anim;
    private bool grounded;
    private float moveVelocity;
    private bool doubleJumped;
    private float climbVelocity;
    private float gravityStore;

    void Start()
    {
        this.playerRigidbody = GetComponent<Rigidbody2D>();
        this.anim = GetComponent<Animator>();
        this.gravityStore = this.playerRigidbody.gravityScale;
        spriteRenderer = GetComponent<SpriteRenderer>();
        onLadder = false;
    }

    private void FixedUpdate()
    {
        this.grounded = Physics2D.OverlapCircle(this.groundCheck.position, this.groundCheckRadius, this.whatIsGround);
    }

    private void Update()
    {
        this.moveVelocity = moveSpeed * Input.GetAxisRaw("Horizontal");
        Move(moveVelocity);

        Climb();

        Jump();

        if (this.knockbackCount <= 0)
        {
            this.playerRigidbody.velocity = new Vector2(this.moveVelocity, this.playerRigidbody.velocity.y);
        }
        else
        {
            if (this.knockFromRight)
            {
                this.playerRigidbody.velocity = new Vector2(-this.knockback, this.knockback);
            }
            else
            {
                this.playerRigidbody.velocity = new Vector2(this.knockback, this.knockback);
            }
            this.knockbackCount -= Time.deltaTime;
        }
    }

    public void Jump()
    {
        if (grounded)
        {
            this.doubleJumped = false;                      //착지하면 더블점프 초기화
        }

        this.anim.SetBool("Grounded", this.grounded);       //착지,점프 애니메이션

        if (Input.GetButtonDown("Jump") && this.grounded)   //착지상태이면 점프 가능
        {
            this.playerRigidbody.velocity = new Vector2(this.playerRigidbody.velocity.x, this.jumpHeight);
            this.anim.SetBool("Grounded", grounded);
        }

        if (Input.GetButtonDown("Jump") && !this.doubleJumped && !this.grounded)        //더블점프
        {
            this.playerRigidbody.velocity = new Vector2(this.playerRigidbody.velocity.x, this.jumpHeight);
            this.anim.SetBool("Grounded", grounded);
            this.doubleJumped = true;
        }
    }

    private void Move(float h)
    {
        bool flipSprite = (spriteRenderer.flipX ? (h > 0.01f) : (h < 0.0f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        this.playerRigidbody.velocity = new Vector2(this.moveVelocity, this.playerRigidbody.velocity.y);

        this.anim.SetFloat("IsWalking", Mathf.Abs(this.playerRigidbody.velocity.x));      //이동 애니메이션

    }

    private void Climb()
    {
        if (this.onLadder)
        {
            this.playerRigidbody.gravityScale = 0f;
            this.climbVelocity = this.climbSpeed * Input.GetAxisRaw("Vertical");
            anim.SetFloat("IsClimbing", Mathf.Abs(climbVelocity));
            this.playerRigidbody.velocity = new Vector2(this.playerRigidbody.velocity.x, this.climbVelocity);
        }
        else
        {
            anim.SetFloat("IsClimbing", 0f);
            this.playerRigidbody.gravityScale = this.gravityStore;
        }

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = other.transform;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }



}
