﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    private const string _gameLevel1 = "Level01";
    private const string _bossLevel1 = "Boss";
    private const string _mainTitle = "TitleMenu";

    private int playerLife = 1;
    private int currentPlayerScore = 0;
    private int playerCurrentHealth = 1;
    private int playerMaxHealth = 1;

    public void OnClickStartGame()
    {
        PlayerPrefs.SetInt("PlayerCurrentLives", playerLife);
        PlayerPrefs.SetInt("CurrentPlayerScore", currentPlayerScore);
        PlayerPrefs.SetInt("PlayerCurrentHealth", this.playerCurrentHealth);
        PlayerPrefs.SetInt("PlayerMaxHealth", this.playerMaxHealth);

        SceneManager.LoadScene(_gameLevel1, LoadSceneMode.Single);
    }

    public void OnClickQuitGame()
    {
        Application.Quit();
    }
}
