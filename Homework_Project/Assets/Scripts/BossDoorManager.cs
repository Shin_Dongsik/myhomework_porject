﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossDoorManager : MonoBehaviour {

    public string levelToLoad;

    private PlayerController thePlayer;
    private bool onDoor;

    // Use this for initialization
    void Start()
    {
        onDoor = false;
    }

    private void Update()
    {
        if(onDoor && Input.GetAxisRaw("Vertical") > 0)
        {
            Debug.Log("보스스테이지 로드!");
            SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            onDoor = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            onDoor = false;
        }
    }
}
